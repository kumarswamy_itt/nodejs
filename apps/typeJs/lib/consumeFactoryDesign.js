"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var factory = __importStar(require("./factoryDesign"));
var productLaptop = factory.createProduct("Prod1", "Laptop");
console.log(productLaptop.getProduct() + "initial count-->", productLaptop.checkProdCount());
productLaptop.updateProdCount(5);
var desktop = factory.createProduct("Prod2", "Desktop");
console.log(desktop.getProduct() + "-->initial count" + desktop.checkProdCount());
desktop.updateProdCount(3);
console.log(productLaptop.getProduct() + "-->updated count" + productLaptop.checkProdCount());
console.log(desktop.getProduct() + "-->updated count" + desktop.checkProdCount());
//# sourceMappingURL=consumeFactoryDesign.js.map