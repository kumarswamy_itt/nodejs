"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var singleton = __importStar(require("./singletonDesign"));
var singletn = singleton.Singleton.getInstance();
console.log("singletn--->", singletn);
singletn.doubleIt();
var singletnNew = singleton.Singleton.getInstance();
singletnNew.doubleIt();
console.log("singletnNew--->", singletnNew);
//# sourceMappingURL=consumeSingleton.js.map