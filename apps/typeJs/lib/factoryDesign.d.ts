export interface Product {
    id: string;
    description: string;
    checkProdCount(): number;
    updateProdCount(productGenerated: number): void;
    getProduct(): String;
}
export declare function createProduct(id: string, description: string): Product;
