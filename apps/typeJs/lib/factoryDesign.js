"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Events = require('events');
var ProductImpl = /** @class */ (function () {
    function ProductImpl(id, description) {
        this.id = id;
        this.description = description;
        this.count = 10;
    }
    ProductImpl.prototype.checkProdCount = function () {
        return this.count;
    };
    ProductImpl.prototype.getProduct = function () {
        return this.description;
    };
    ProductImpl.prototype.updateProdCount = function (productGenerated) {
        this.count = this.count + productGenerated;
    };
    return ProductImpl;
}());
function createProduct(id, description) {
    return new ProductImpl(id, description);
}
exports.createProduct = createProduct;
//# sourceMappingURL=factoryDesign.js.map