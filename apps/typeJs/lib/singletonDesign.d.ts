export declare class Singleton {
    num: number;
    private static object;
    private constructor();
    static getInstance(): Singleton;
    doubleIt(): void;
}
