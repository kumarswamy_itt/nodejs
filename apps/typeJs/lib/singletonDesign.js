"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Singleton = /** @class */ (function () {
    function Singleton() {
        this.num = 0;
        this.num = 10;
        console.log();
    }
    Singleton.getInstance = function () {
        if (!Singleton.object) {
            Singleton.object = new Singleton();
        }
        return Singleton.object;
    };
    Singleton.prototype.doubleIt = function () {
        console.log("multiplying the property num by 2");
        this.num *= 2;
    };
    return Singleton;
}());
exports.Singleton = Singleton;
//# sourceMappingURL=singletonDesign.js.map