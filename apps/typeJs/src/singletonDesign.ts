export class Singleton {
    num: number = 0;
    private static object:Singleton;
    private constructor() {
        this.num = 10;
        console.log()
    }

    static getInstance() {
        if (!Singleton.object) {
            Singleton.object = new Singleton();
        }
        return Singleton.object;
    }

    doubleIt(){
        console.log("multiplying the property num by 2");
        this.num *=2;
    }
}